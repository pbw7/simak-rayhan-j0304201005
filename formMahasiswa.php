<?php 
include 'header.php';

      $edit = false;
      if (!empty($_GET['nim'])) {
      $sql = 'SELECT * FROM mahasiswa WHERE nim="'.$_GET['nim'].'"';

      $query = mysqli_query($conn, $sql);

      if (mysqli_num_rows($query)) {
        $edit = true;
        $data = mysqli_fetch_object($query);

      }

  }

?>
<h1>Form Mahasiswa</h1>
<form method="post" action="saveMahasiswa.php">
  <div class="mb-3">
    <label class="form-label" class="form-label">NIM</label>
    <input type="text" class="form-control"  placeholder = "NIM" name="nim" value="<?php if ($edit) echo $data->nim; ?>" required>
    <input type = "hidden" name="id" value="<?php if ($edit) echo $data->nim; ?>">
  </div>
  <div class="mb-3">
    <label class="form-label" class="form-label">Nama</label>
    <input type="text" class="form-control" placeholder = "Nama"  name="nama" value="<?php if ($edit) echo $data->nama; ?>" required>
  </div>
  <div class="mb-3">
    <label class="form-label" class="form-label">Jenis kelamin</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="jenis_kelamin" value="Pria" id="Pria" checked>
        <label class="form-check-label" for="Pria">
            Pria
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="jenis_kelamin" value="Wanita" id="Wanita" 
          <?php if ($edit && $data->jenis_kelamin == 'Wanita') echo 'checked'; ?>>
        <label class="form-check-label" for="Wanita">
            Wanita
        </label>
    </div>
<div class="mb-3">
    <label class="form-label" class="form-label">Tanggal Lahir</label>
    <input type="date" class="form-control" placeholder = "Tanggal Lahir" name="tanggal_lahir" value="<?php if ($edit) echo $data->tanggal_lahir; ?>" required>
  </div>
  <div class="mb-3">
    <label class="form-label" class="form-label">Alamat</label>
    <textarea  class="form-control" placeholder = "Alamat" name="alamat" required><?php if ($edit) echo $data->alamat; ?> </textarea>
  </div>

  <div class="mb-3">
    <label class="form-label" class="form-label">Program Studi</label>
    <select class="form-control" placeholder="Program Studi" name="id_prodi" required>

        <?php
        $sql = 'SELECT * FROM prodi';
        $query = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_object($query)) {
            ?>
            <option value="<?php echo $row->id_prodi; ?>" <?php if ($edit && $data->id_prodi == $row->id_prodi) echo 'selected' ?>> <?php echo $row->nama_prodi; ?></option>



        <?php } ?> 

        <!-- <Option value="1">Komunikasi</option>
        <Option value="2">Ekowisata</option>
        <Option value="1">Manajemen Informatika</option> -->

    </select>
    <div class="mb-3 mt-3" >
        <input type = "submit" class = "btn btn-warning btn-success" value="Simpan">
  </div>


</form>
<?php include 'footer.php';?>