<?php include 'header.php'; ?>

<h1 class="mt-3 mb-3">Form Login</h1>
<form action="login.php" method="POST">
    <div class="mb-3">
        <label class="form-label">Username</label>
        <input type="text" class="form-control" name="username" placeholder="Username" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Password</label>
        <input type="password" class="form-control" name="password" placeholder="Password" required>
    </div>
    <div class="mb-3">
        <input type="submit" value="Masuk" class="btn btn-sm btn-success">
    </div>
</form>

<?php include 'footer.php'; ?>    