<?php include 'connect.php'; ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIMAK | Mahasiswa | Prodi</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg bg-warning">
        <div class="container-fluid container">
            <a class="navbar-brand" href="index.php">SIMAK</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.php">Mahasiwa</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="index2.php">Program Studi</a>
                </li>
            </ul>

                <?php if (empty($_SESSION['username'])) { ?>

                <a href="formLogin.php" class="btn btn-light">Masuk<a/>
                
                <?php } else { ?>

                    <a href="logout.php" class="btn btn-danger">Keluar<a/>    
                <?php } ?>
               
            </div>
        </div>
    </nav>
    <div class="container pt-5 pb-5">